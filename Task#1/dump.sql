# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 0.0.0.0 (MySQL 5.7.22)
# Database: vh
# Generation Time: 2019-06-25 08:52:04 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;

INSERT INTO `category` (`id`, `category_name`)
VALUES
	(1,'News'),
	(2,'Fun'),
	(3,'Technology'),
	(4,'Animals');

/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table like
# ------------------------------------------------------------

DROP TABLE IF EXISTS `like`;

CREATE TABLE `like` (
  `post_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`post_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `like` WRITE;
/*!40000 ALTER TABLE `like` DISABLE KEYS */;

INSERT INTO `like` (`post_id`, `user_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `like` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table post
# ------------------------------------------------------------

DROP TABLE IF EXISTS `post`;

CREATE TABLE `post` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post` varchar(81) DEFAULT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `use_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `idxPostToCategory` (`category_id`),
  KEY `idxUserToPost` (`use_id`),
  CONSTRAINT `idxPostToCategory` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  CONSTRAINT `idxUserToPost` FOREIGN KEY (`use_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;

INSERT INTO `post` (`id`, `post`, `category_id`, `use_id`)
VALUES
	(1,'Test Test Test',1,1),
	(2,'New awsome post',3,1);

/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(40) NOT NULL DEFAULT '',
  `login` varchar(50) DEFAULT NULL,
  `password` varchar(100) NOT NULL DEFAULT '',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `email`, `login`, `password`, `is_active`)
VALUES
	(1,'test@yandex.ru','test','NGUi47h23uf',1),
	(2,'reger@yann.ru','er','fewfw',0),
	(3,'fhewuifhui@ya.ru','fnuewhf','bjwbfwe',0),
	(4,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(5,'5fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(6,'6fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(7,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(8,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(9,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(10,'10fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(11,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(12,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(13,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(14,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(15,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(16,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(17,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(18,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(19,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(20,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(21,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(22,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(23,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(24,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(25,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(26,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(27,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(28,'5fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(29,'6fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(30,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(31,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(32,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(33,'10fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(34,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(35,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(36,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(37,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(38,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(39,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(40,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(41,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(42,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(43,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(44,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(45,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(46,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(47,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(48,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(49,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(50,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(51,'5fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(52,'6fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(53,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(54,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(55,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(56,'10fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(57,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(58,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(59,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(60,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(61,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(62,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(63,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(64,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(65,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(66,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(67,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(68,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(69,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(70,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(71,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(72,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(73,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(74,'5fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(75,'6fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(76,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(77,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(78,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(79,'10fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(80,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(81,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(82,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(83,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(84,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(85,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(86,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(87,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(88,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(89,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(90,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(91,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(92,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(93,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(94,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(95,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(96,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(97,'5fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(98,'6fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(99,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(100,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(101,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(102,'10fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(103,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(104,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(105,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(106,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(107,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(108,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(109,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(110,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(111,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(112,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(113,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(114,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(115,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(116,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(117,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(118,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(119,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(120,'5fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(121,'6fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(122,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(123,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(124,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(125,'10fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(126,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(127,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(128,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(129,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(130,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(131,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(132,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(133,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(134,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(135,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(136,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(137,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(138,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(139,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(140,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(141,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(142,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(143,'5fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(144,'6fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(145,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(146,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(147,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(148,'10fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(149,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(150,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(151,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(152,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(153,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(154,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(155,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(156,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(157,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(158,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(159,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(160,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(161,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(162,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(163,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(164,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(165,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(166,'5fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(167,'6fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(168,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(169,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(170,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(171,'10fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(172,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(173,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(174,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(175,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(176,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(177,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(178,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(179,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(180,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(181,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(182,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(183,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(184,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(185,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(186,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(187,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(188,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(189,'5fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(190,'6fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(191,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(192,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(193,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(194,'10fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(195,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(196,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(197,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(198,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(199,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(200,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(201,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(202,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(203,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(204,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(205,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(206,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(207,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(208,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(209,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(210,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(211,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(212,'5fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(213,'6fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(214,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(215,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(216,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(217,'10fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(218,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(219,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(220,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(221,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(222,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(223,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(224,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(225,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(226,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(227,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(228,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(229,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(230,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(231,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(232,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(233,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(234,'fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(235,'5fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0),
	(236,'6fgerg@yyaaa.ry','fenwiofj','bewjbfwe',0);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
