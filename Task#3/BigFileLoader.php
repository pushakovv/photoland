<?php

namespace FileReader;

require_once __DIR__ . 'FileReader.php';

use SeekableIterator;
use OutOfBoundsException;


class BigFileLoader implements SeekableIterator {
    private $position;

    /**
     * @var FileReader
     */
    private $file;

    public function __construct($path)
    {
        $this->file = FileReader::getInstance($path);
    }

    public function seek($position)
    {
        if ($position < 0 || $position >= $this->file->getCount()) {
            throw new OutOfBoundsException("Invalid seek position ($position)");
        }
        $this->position = $position;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * @return mixed|string
     * @throws \Exception
     */
    public function current()
    {
        return $this->file->getString($this->position);
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return $this->position < $this->file->getCount();
    }
}

// POC
$filePath = __DIR__ . 'files/bigFile.txt';
$bigFileReader = new BigFileLoader($filePath);
print($bigFileReader->current());