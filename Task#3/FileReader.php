<?php

namespace FileReader;

use Exception;

class FileReader
{
    private static $instances = [];

    private $handle;
    private $strings = [];
    private $count;
    private $chunk = 4096;

    public function __construct($path)
    {
        $this->count = 0;
        $this->handle = fopen($path, "r");
        $this->definitionStrings();
    }

    public static function getInstance($path)
    {
        if (!array_key_exists($path, self::$instances)) {
            self::$instances[$path] = new self($path);
        }
        return self::$instances[$path];
    }

    private function definitionStrings()
    {
        while (fgets($this->handle, $this->chunk) !== false) {
            $this->strings[] = ftell($this->handle);
            ++$this->count;
        }
    }

    public function getCount(){
        return $this->count;
    }

    public function getString($index) {
        if ($index >= $this->getCount()) {
            throw new Exception('Excess max count of string');
        }

        if ($index === 0){
            fseek($this->handle, 0);
        } else {
            fseek($this->handle, $this->strings[$index - 1]);
        }

        return rtrim(fgets($this->handle, $this->chunk), PHP_EOL);
    }
}