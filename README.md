###PhotoLand - [Тестовое задание](https://docs.google.com/document/d/10K4oh8ZYxfetu8diMg12ysQdMNRYp_1JW3U_8m_RVzw/edit)

## 1. Задание
```Task#1/dump.sql``` - содержит структуру таблиц для следующих запросов:

1. Запрос на постановку лайка от юзера к новости - ```INSERT INTO `like` (`post_id`, `user_id`) VALUES (`1`, `1`)```
2. Запрос на отмену лайка - ```DELETE FROM `like` WHERE (`post_id` = `2` AND `user_id` = `1`)```
3. Выборка пользователей, оценивших новость, желательно учесть что их могут быть тысячи и сделать возможность постраничного вывода - ```SELECT u.`email`, u.`login`  FROM `like` as l
                 JOIN `user` AS u ON l.`user_id` = u.`id`
                 WHERE l.`post_id` = 1
                 LIMIT 50```
4. Запрос для вывода ленты новостей - ```SELECT p.`id`, p.`post`, c.`category_name` FROM `post` AS p JOIN `category` AS c ON p.`category_id` = c.`id` ORDER BY `id` DESC LIMIT 50```
5. Запрос на добавление поста в ленту - ``` INSERT INTO `post` (`post`, `category_id`, `use_id`) VALUES (`New awsome post`, `3`, `1`)```


## 2. Задание

Скрипт для подсчета кол-ва доменов лежит в директории ```Task#2```

## 3. Задание

Класс для загрузки файлой находится в директории ```Task#3```
