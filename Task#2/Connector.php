<?php


namespace EmailDomainCounter;

use PDOException;
use PDO;


/**
 * Class Connector
 */
class Connector
{
    protected static $instance;

    /**
     * @return PDO
     */
    public static function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        return self::$instance = self::getDbConnection();
    }

    /**
     * @return PDO
     */
    private static function getDbConnection()
    {
        try {
            $host = 'localhost';
            $dbName = 'vh';
            $login= 'root';
            $pass = 'root';
            $port = 8989;

            $dsn = "mysql:host=" . $host . ";dbname=" . $dbName . ";charset=utf8;port=" . $port;

            return new PDO($dsn, $login, $pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function __construct()
    {
        // Disable constructor
    }

    public function __clone()
    {
        // Disable clone
    }

    public function __wakeup()
    {
        // Disable wakeup
    }
}