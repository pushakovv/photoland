<?php

namespace EmailDomainCounter;

require_once __DIR__ . 'Connector.php';

use PDO;


class EmailDomainCounter
{
    private $domainList = [];
    private $domainRegExp = '/@(([-A-Za-z0-9]{1,}\.){1,2}[-A-Za-z]{2,})$/';
    // explain показал что оператор BETWEEN аффектит меньше строк чем лимит (что логично),
    // поэтому "выгоднее использовать его"
    private $sql = 'SELECT `email` FROM users WHERE `id` BETWEEN :start AND :limit AND LENGTH(`email`) > 7';

    private $db;

    public function __construct(PDO $connector)
    {
        $this->db = $connector;
    }

    public function calculate()
    {
        $limit = 500;
        $condition = [':limit' => $limit, ':start' => 1];
        $stm = $this->db->prepare($this->sql);

        while ($stm->execute($condition)) {
            foreach (($data = $stm->fetchAll(PDO::FETCH_ASSOC)) as $row) {
                if (preg_match_all($this->domainRegExp, $row, $res)) {
                    foreach ($res as $email) {
                        isset($this->domainList[$email[1]])
                            ? $this->domainList[$email[1]]++
                            : $this->domainList[$email[1]] = 1;
                    }
                }

            }

            if (!$data) {
                return false;
            }

            $condition[':start'] += $limit;
            $condition[':limit'] += $limit;
        }
    }

    public function count()
    {
        var_dump($this->domainList);
    }
}

$emailDomainCounter = new EmailDomainCounter(Connector::getInstance());
$emailDomainCounter->calculate();
$emailDomainCounter->count();